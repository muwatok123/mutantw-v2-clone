// firmware V2.1 for mutantW v2
// Install this libs before building the firmware
#include <Adafruit_ST7789.h> 
#include <SPI.h>
#include <WiFi.h>
#include "time.h"
#include <Wire.h>
#include "RTClib.h"

// Set the hotspot pass you will use to get the time from the internet
const char* ssid     = "DESKTOP";
const char* password = "84r19+H8";

// You can change the gmtOffset_sec to set your local time
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = -14400;
const int   daylightOffset_sec = 0;

#define BUTTON_PIN_BITMASK 0x3000 // 2^33 in hex
RTC_DATA_ATTR int bootCount = 0;

// For LCD
#define TFT_CS         34
#define TFT_RST        37
#define TFT_DC         33
#define TFT_MOSI       35  // Data out
#define TFT_SCLK       36  // Clock out

// For buttons and others
const int LITE = 38;
const int UP = 13;
const int DOWN = 12;
const int VIBRATION = 11;
const int BATTERY = 10;
const int INT_IMU = 9;
const int TOUCH = 5;


// For buttons and others
int touchState = 1;
static bool upState = 0;
static bool downState = 1;
static uint16_t powered_on = 10000;

// For Display turn off
unsigned long DELAY_TIME = 10000; // 10 sec
unsigned long delayStart = 0; // the time the delay started

// For LCD
Adafruit_ST7789 tft = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

RTC_DS1307 rtc;

static void notification_Vibration(void)
{
  Serial.println("notification_Buzzer");
  digitalWrite(VIBRATION, HIGH);
  delay(1000);
  digitalWrite(VIBRATION, LOW);
  delay(800);
  digitalWrite(VIBRATION, HIGH);
  delay(1000);
  digitalWrite(VIBRATION, LOW);
}

void watchFace() {
  DateTime now = rtc.now();
  
  tft.setCursor(80, 30);
  tft.fillScreen(ST77XX_BLACK);
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(13.5);
  tft.println(now.hour());
  tft.setTextColor(ST77XX_YELLOW);
  tft.setTextSize(13.5);
//  tft.println(':');
  tft.print(now.minute());
}

void secondFace() {
  DateTime now = rtc.now();
  tft.setCursor(0, 30);
  tft.fillScreen(ST77XX_BLACK);
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(2);
  tft.println(now.day());
  tft.setTextColor(ST77XX_YELLOW);
  tft.setTextSize(2);
  tft.println(now.month());
  tft.setTextColor(ST77XX_WHITE);
  tft.setTextSize(2);
  tft.println(now.year());
  tft.setTextSize(3);
  tft.setTextColor(ST77XX_GREEN);
  tft.setTextSize(5);
  tft.println(now.hour());
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(4);
  tft.println(now.minute());
  tft.setTextColor(ST77XX_WHITE);
  tft.setTextSize(4);
  tft.println(now.second());

  int analogValue = analogRead(BATTERY);
  int analogVolts = analogReadMilliVolts(2);
  
  // print out the values you read:
  Serial.printf("ADC analog value = %d\n",analogValue);
  Serial.printf("ADC millivolts value = %d\n",analogVolts);
}

void notificationFace() {

  tft.setCursor(10, 30);
  tft.fillScreen(ST77XX_BLACK);
  
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(2);
//  tft.println(&timeinfo, "%H:%M:%S");
  
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(2);
//  tft.println( myData.a);
  
  tft.setTextColor(ST77XX_YELLOW);
  tft.setTextSize(2);
//  tft.println( myData.b);

  notification_Vibration();
}

void setupWIFITime(){
  tft.setCursor(0, 30);
  tft.fillScreen(ST77XX_BLACK);
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(1);
  // Connect to Wi-Fi
  tft.println(" mutantW v2 ");
  tft.println(" SETUP TIME FOR FIRST TIME ");
  Serial.print("Connecting to ");
  tft.println("Connecting to ");
  tft.println(ssid);
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    tft.print(".");
  }
  Serial.println("");
  tft.println("");
  tft.println("WiFi connected.");
  Serial.println("WiFi connected.");

  // Init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %d %B %Y %H:%M:%S");

  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
}

static void turnDisplayON(void)
{
  if (!digitalRead(LITE)) {
    digitalWrite(LITE, HIGH);
  }
}
  
void setup(void) {
  Serial.begin(115200);
  
  delay(1000); //Take some time to open up the Serial Monitor
  
  Wire.begin(7, 6);

  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(LITE, OUTPUT);
  pinMode(VIBRATION, OUTPUT);
  
  analogReadResolution(12);

  // Set Switch State
  digitalWrite(LITE, HIGH);

  //Increment boot number and print it every reboot
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));
  esp_sleep_enable_ext1_wakeup(BUTTON_PIN_BITMASK,ESP_EXT1_WAKEUP_ANY_HIGH); //1 = High, 0 = Low

  tft.init(240, 280);           // Init ST7789 240x240
  tft.setTextWrap(false);

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }
  
  if(bootCount<2){
    // First boot
//    setupWIFITime();
    delay(1000);
    
    Serial.println("Set time to RTC ");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
//    rtc.adjust(DateTime(timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec));
  } else{
    Serial.println("Get time from RTC ");
    DateTime now = rtc.now();

    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);

    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();
  }

  watchFace();

  delayStart = millis();
}

void loop() {
  /* power off the display after 6s */
  if ((millis() - delayStart) >= DELAY_TIME) {
      digitalWrite(LITE, LOW);
      upState =1;
      downState=1;
      Serial.println("Going to sleep now");
      Serial.flush(); 
      esp_deep_sleep_start();
      Serial.println("This will never be printed");
  }

  if (digitalRead(UP)) {
    Serial.println("UP_BTN");
    delayStart = millis();
    turnDisplayON();
    if (upState == 1){
      Serial.println("1");
      upState =0;
      watchFace();
      delay(500);
    } else{
      upState =1;
      digitalWrite(LITE, LOW);
      Serial.println("0");
      delay(500);
    }
  }

  if (digitalRead(DOWN)) {
    Serial.println("UP_DOWN");
    delayStart = millis();
    turnDisplayON();
    if (downState == 1){
      Serial.println("1");
      downState =0;
      secondFace();
      delay(500);
    } else{
      downState =1;
      notificationFace();
      Serial.println("0");
      delay(500);
    }
  }
}


// written by
// rahmanshaber @rahmanshaber

